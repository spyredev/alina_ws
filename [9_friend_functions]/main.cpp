#include <iostream>
#include <string.h>

using namespace std;


class Persoana{
public:
    int varsta;
    void setNume(char * n);
    char * getNume();
    
    void afisare();
    
    friend void afisarePersoana(Persoana * p);
    
private:
    char * nume;
    
};



char * Persoana::getNume(){
    
    return nume;
    
}

void Persoana::setNume(char *n){
    nume = new char[strlen(n)+1];
    strcpy(nume, n);
    
}

void Persoana::afisare(){
    cout <<"Nume: " << nume << " varsta: " << varsta << endl;
}




void afisarePersoana(Persoana * p){
    cout <<"NUME: " << p->nume << " VARSTA: " << p->varsta<<endl;
}

int main(){
    
    Persoana *p1 = new Persoana();
    p1->setNume("Jimmy");
    p1->varsta = 33;
    
    Persoana * p2 = new Persoana();
    p2->setNume("Freeman");
    p2->varsta = 60;
    
    p1->afisare();
    p2->afisare();
    
    afisarePersoana(p1);
    
    return 0;
}