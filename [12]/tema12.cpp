#include <iostream>
#include <string.h>

using namespace std;

class Entitate_Comerciala{
public:

    /* cum fac bugetul obligatoriu ?  (Nu merge asa!)
    Entitate_Comerciala(double b){
        buget = b;
    }
    */
    Entitate_Comerciala(double b){
        buget = b;
    }



    void setVenit(double v){
        venit = v;
    }

    double getVenit(){
        return venit;
    }

    void setCheltuiala(double c){
        cheltuiala = c;
    }

    double getCheltuiala(){
        return cheltuiala;
    }

    void afisare(){
        cout << "Venit = " << venit << " si " << "Cheltuiala = " << cheltuiala << endl;
    }

    void plateste_taxe(){
        cout << "Bugetul este : " << venit - cheltuiala << endl;
    }

private:
    double venit;
    double cheltuiala;
    double buget;
};

class Firma : public Entitate_Comerciala{
public:
    int CUI;

    Firma(){
    }

    Firma(int x){
        CUI = x;
    }

    void afisare(){
        cout << "Venit: " << getVenit() << " ," << " cheltuiala: " << getCheltuiala() << " ," << " CUI: " << CUI << endl;
    }
};

class PFA : public Entitate_Comerciala{
public:
    int CNP;

    PFA(){
    }

    PFA(int y){
        CNP = y;
    }

    void afisare(){
        cout << "Date PFA => " << "Venit: " << getVenit() << " ," << " cheltuiala: " << getCheltuiala() << " ," << " CNP: " << CNP << endl;
    }
};

class Text1{
public:
    static void afisareSeparator1(){
        cout << "========================================================" << endl;
    }
};

class Text2{
public:
    static void afisareSeparator2(){
        cout << "\n********************************************************\n" << endl;
    }
};

int main()
{
    cout << "Date entitate comerciala 1 : " << endl;

    Entitate_Comerciala e1;
    e1.setVenit(4000);
    e1.setCheltuiala(3000);
    e1.afisare();
    e1.plateste_taxe();

    Text1::afisareSeparator1();

    cout << "Date entitate comerciala 2 : " << endl;

    Entitate_Comerciala * e2 = new Entitate_Comerciala();
    e2->setVenit(6500);
    e2->setCheltuiala(7000);
    e2->afisare();
    e2->plateste_taxe();

    Text2::afisareSeparator2();

    cout << "Date firma 1 : " << endl;

    Firma f1;
    f1.CUI = 21758;
    f1.setVenit(100000);
    f1.setCheltuiala(50500);
    f1.afisare();

    Text1::afisareSeparator1();

    cout << "Date firma 2 : " << endl;

    Firma * f2 = new Firma();
    f2->setVenit(60900);
    f2->setCheltuiala(60880);
    f2->CUI = 14635;
    f2->afisare();
    f2->plateste_taxe();

    Text2::afisareSeparator2();

    PFA * p = new PFA();
    p->CNP = 1951023;
    p->setVenit(60000);
    p->setCheltuiala(1700);
    p->afisare();
    p->plateste_taxe();

    Text1::afisareSeparator1();

    p = new PFA(2980422);
    p->setVenit(4000);
    p->setCheltuiala(3890);
    p->afisare();
    p->plateste_taxe();

    cout << endl;

    return 0;
}
