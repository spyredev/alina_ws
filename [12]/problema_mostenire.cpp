#include <iostream>
#include <string.h>

using namespace std;

class Entitate_Comerciala{
public:

    /* cum fac bugetul obligatoriu ?  (Nu merge asa!)
    Entitate_Comerciala(double b){
        buget = b;
    }
    */
    Entitate_Comerciala(double b){
        buget = b;
    }

    


    void setVenit(double v){
        venit = v;
    }

    double getVenit(){
        return venit;
    }

    void setCheltuiala(double c){
        cheltuiala = c;
    }

    double getCheltuiala(){
        return cheltuiala;
    }

    virtual void afisare(){
        cout << "DATE ENTITATE COMERCIALA Venit = " << venit << " si " << "Cheltuiala = " << cheltuiala << endl;
    }

    // virtual void plateste_taxe() = 0;

    void plateste_taxe(){
        cout << "Bugetul este : " << venit - cheltuiala << endl;
    }

private:
    double venit;
    double cheltuiala;
    
protected:
    double buget;
};

class Firma : public Entitate_Comerciala{
public:
    int CUI;

    Firma(double bud) : Entitate_Comerciala(bud){
        
    }

    Firma(int x, double bud) : Entitate_Comerciala(bud){
        CUI = x;
    }

    void afisare(){
        cout << "DATE FIRMA Venit: " << getVenit() << " ," << " cheltuiala: " << getCheltuiala() << " ," << " CUI: " << CUI << " BUGET = " << buget <<endl;
    }
    
    void plateste_taxe(){
        
    }
};


class PFA : public Entitate_Comerciala{
public:
    int CNP;

    PFA(double sushi) : Entitate_Comerciala(sushi){
        
    }

    PFA(int y, double pizza) : Entitate_Comerciala(pizza){
        CNP = y;
    }

    void afisare(){
        cout << "Date PFA => " << "Venit: " << getVenit() << " ," << " cheltuiala: " << getCheltuiala() << " ," << " CNP: " << CNP << " BUGET = " << buget << endl;
    }
};



int main(){
    
    
    
    Entitate_Comerciala * ec = new Entitate_Comerciala(10000);
    
    Firma * firma1 = new Firma(9000);
    Firma * firma2 = new Firma(328109321, 12000);
    
    PFA * pfa1 = new PFA(23500);
    
    ec->afisare();
    pfa1->afisare();
    firma1->afisare();
    firma2->afisare();
    
    // vector 
    cout <<"===============================vector=de=firme==================================\n";
    
    Firma * firme[100];
    // Firma  oFirma;
    firme[0] = firma1;
    firme[1] = firma2;
    
    Firma * firma3 = new Firma(100000);
    firme[2] = firma3;
    
    firme[3] = new Firma(200000);
    firme[4] = new Firma(333000);
    
    // firme[0]->afisare();
    for(int i=0; i<4; i++){
        firme[i]->afisare();
    }
    
    
    cout <<"==============================vector=de=orice=tip=Firma==EC==PFA=============================\n";
    // Entitate_Comerciala * entitate = new Entitate_Comerciala(20);
    // entitate->afisare();
    // entitate = new Firma(3000);
    // entitate->afisare();
    
    Entitate_Comerciala * entitati[1000];
    entitati[0] = firma1;
    entitati[1] = firma2;
    entitati[2] = pfa1;
    entitati[3] = ec;
    
    for(int i=0; i<4; i++){
        entitati[i]->afisare();
    }
    
    return 0;
}