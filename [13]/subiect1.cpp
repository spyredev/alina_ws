#include <iostream>
#include <string.h>

using namespace std;

class Medicament{
public:
    Medicament(char * denumire, float pret){
        this->pret = pret;
        this->denumire = new char[strlen(denumire)+1];
        strcpy(this->denumire, denumire);
    }
    
    Medicament(){
        
    }
    
    void afisare(){
        cout <<"Nume medicament: " << denumire << " pret: " << pret << endl;
    }
    
    void setDenumire(char * denumire){
        this->denumire = new char[strlen(denumire)+1];
        strcpy(this->denumire, denumire);
    }
    
    void setPret(float pret){
        this->pret = pret;
    }
    
    // - 
    friend ostream &operator<<( ostream &output, 
                                       const Medicament &med )
      { 
         output << "DENUMIRE: " << med.denumire << endl;
        return output;            
      }
      
    friend istream &operator>>( istream &in, 
                                       Medicament &med )
      { 
         cout <<"Introduceti o denumire: ";
         in >> med.denumire;
         cout <<"Introduceti pretul: ";
         in >> med.pret;
        return in;            
      }
      
      char * getDenumire(){
          return this->denumire;
      }
      
      float getPret(){
          return pret;
      }
    
private:
    float pret;
    char * denumire;
};

class Reteta{
public:
    
    Reteta(Medicament * medicamente, int dim){
        
        int i;
        for(i=0; i<dim; i++){
            cout << medicamente[i];
            this->medicamente[i] = &medicamente[i];
        }
        nrMedicamente = dim;
        
    }
    
    Reteta(){
        nrMedicamente = 0;
    }
    
    
    
    void adaugaMedicament(Medicament * m){
        medicamente[nrMedicamente] = m;
        nrMedicamente++;
    }
    
    void afisareMedicamente(){
        cout <<"Afisare medicamente: " << endl;
        int i;
        for(i=0; i<this->nrMedicamente; i++){
            cout << *medicamente[i];
        }
    }
    
    virtual float getValoare(){
        float suma = 0.0f;
        for(int i=0; i<nrMedicamente; i++){
            suma += medicamente[i]->getPret();
        }
        return suma;
    }
    
    friend ostream &operator<<( ostream &output, 
                                       const Reteta &reteta )
      { 
         output<<"ACEASTA ESTE O RETETA CARE CONTINE: "<<endl;
         int i;
        for(i=0; i<reteta.nrMedicamente; i++){
            output << "\t\t"<<reteta.medicamente[i]->getDenumire()<<" " << reteta.medicamente[i]->getPret()<<endl;
        }
        return output;            
      }
    
private:
    Medicament * medicamente[1000];
    int nrMedicamente;
};


class RetetaCompensata : public Reteta{
public:
    RetetaCompensata(float procentCompensat){
        this->procentCompensat = procentCompensat;
    }
    
    virtual float getValoare(){
        float suma = 0.0f;
        // for(int i=0; i<nrMedicamente; i++){
        //     suma += medicamente[i]->getPret();
        // }
        suma = Reteta::getValoare();
        suma = suma * procentCompensat;
        return suma;
    }
    
private:
    float procentCompensat;
};



int main(){

    Medicament nurofen("Nurofen", 11.25f);

    Medicament aspirina = nurofen;
    
    aspirina.setDenumire("Aspirina");
    aspirina.setPret(4.5f);
    
    
    Medicament bixtonim("Bixtonim", 8.2f);
    
    Medicament medicamente[] = {nurofen, aspirina, bixtonim};
    

    Reteta r1(medicamente, 3);
    
    Medicament * antinevralgic = new Medicament("Antinevralgic", 12.22f);
    // cout << *antinevralgic;
    
    Medicament paracetamol("Paracetamol", 9.99f);
    //r1.adaugaMedicament(&paracetamol);
    
    r1.afisareMedicamente();
    
    cout << r1;
    
    RetetaCompensata rc2(0.5f);
    rc2.adaugaMedicament(&bixtonim);
    
    rc2.afisareMedicamente();
    
    // RetetaCompensata rc3(r1, 0.2){ // COPY CONSTRUCTOR
        
    cout <<"TESTARE METODA GETVALOARE" << endl;
    cout << r1.getValoare();
    cout <<" TEST 2: " << endl;
    cout << rc2.getValoare();
    
    return 0;
}










































int main0(){
    
    Medicament nurofen("Nurofen", 11.25f);
    // Medicament nurofen;
    nurofen.afisare();
    Medicament aspirina = nurofen;
    
    aspirina.setDenumire("Aspirina");
    aspirina.setPret(4.5f);
    aspirina.afisare();
    
    
    Medicament bixtonim("Bixtonim", 8.2f);
    
    cout <<"=========================1"<<endl;
    
    Medicament unMedicament("TEST", 99.0f);
    unMedicament.afisare();
    // cin >> unMedicament;
    
    
    unMedicament.afisare();
    
    
    cout <<"=========================2"<<endl;
    
    
    { 
        int x = 30; 
        cout << x<<endl;
    }
    // cout << x << endl; // nu se poate, x este definit mai sus intre {}
    
    cout <<"=========================3"<<endl;
    
    {
        Medicament temp;
        temp = nurofen;
        cout << temp << endl;
    }
    
    cout <<"=========================4"<<endl;
    int * nota = new int[100];
    *nota = 10; // nota[0] = 10;
    nota[1] = 9;
    *(nota+1) = 8;
    cout << *nota<<endl;
    cout << nota[1]<<endl;
    
    
    cout <<"=========================5 - pointeri nasty"<<endl;
    int note[3];
    note[0] = 8;
    note[1] = 9;
    note[2] = 7;
    
    cout << note[0] << endl;
    cout << note << endl;
    
    int * pointer = note;
    
    cout << "Pointer = " << pointer << endl;
    cout <<"Element la adresa = " << *pointer << endl;
    
    cout <<"ELEMENT: " << *(pointer+1) << endl;
    cout <<"END PROGRAM"<<endl;
    
    return 0;
}