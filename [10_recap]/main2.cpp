#include <iostream>
#include <string.h>

using namespace std;

int factorial(int n){
    cout <<n << "! = ";
    int rezultat = 1;
    int i;
    for(i=1; i<=n; i++){
        rezultat = rezultat * i;
    }
    cout << rezultat;
    cout << endl;
    return rezultat;
}

int suma(int x, int y){
    // cout <<"Suma dintre " << x<< " si " << y << " este " << x+y <<endl;
    return x+y;
}

int suma(int x, int y, int z){
    return x+y+z;
}

int main(){
    
    
    
    // 7! + 12! + 5!
    int n1 = 7;
    int n2 = 12;
    int n3 = 5;
    
    int s = 0;
    
    s += factorial(n1);
    s += factorial(n2);
    s += factorial(n3);
    cout <<"Suma pentru forex este: " << s << endl;
    cout <<"==================================================="<<endl;
    // int rez = suma(2,3);
    // cout << "The sum is: " <<rez <<endl;
    
    cout << suma(1,2) << endl;
    
    cout <<"==================================================="<<endl;
    cout << suma(suma(1,1), suma(2,2)) << endl;
    
    cout <<"==================================================="<<endl;
    cout << suma(suma(n1,n2), suma(n3, 0)) << endl;
    cout << suma(suma(n1,n2), n3) << endl;
    
    cout <<"==================================================="<<endl;
    cout << suma(n1, n2, n3) << endl;
    
    
}