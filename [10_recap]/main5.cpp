#include <iostream>
#include <string.h>

using namespace std;

void modifica(int * x){
    *x = (*x)+100;
    
}

void modificaRef(int & x){
    x = x+100;
}


class Persoana{
    public:
        int varsta;
};


void modificareVarsta(Persoana & p){
    p.varsta = -300;
}

int main(){
    
    int numar = 300;
    // modifica(&numar);
    modificaRef(numar);
    cout << numar << endl;
    
    
    Persoana *p1 = new Persoana();
    p1->varsta = 20;
    
    cout << p1->varsta << endl;
    modificareVarsta(*p1);
    cout << p1->varsta << endl;
    
    Persoana p2;
    p2.varsta = 23;
    cout << p2.varsta << endl;
    modificareVarsta(p2);
    cout << p2.varsta << endl;
    
    return 0;
}