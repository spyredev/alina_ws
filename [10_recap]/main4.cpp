#include <iostream>
#include <string.h>

using namespace std;

class Book{
public:

    Book(){
        cout <<"NEW BOOK!!!!"<<endl;
    }

    void setTitle(char * title){
        this->title = new char[strlen(title)+1];
        strcpy(this->title, title);
    }    
    
    char * getTitle(){
        return title;
    }
    
    void afisare(){
        cout <<"Cartea se cheama: " << title << endl;
    }
    
    friend void displayBookDE(Book * bk);
private:
    char * title;
};


void displayBookDE(Book * bk){
    cout <<"Das Buch heisst " <<bk->title<<endl;   
}



int main(){
    
    
    Book * b1 = new Book();
    b1->setTitle("I, Robot");
    b1->afisare();
    
    
    displayBookDE(b1);
    
    
    cout <<"==============================" << endl;
    Book b2;
    b2.setTitle("Avatar");
    
    Book * b3 = &b2;
    b3->setTitle("Capn Phillips");
    
    b2.afisare();
    
    return 0;
}