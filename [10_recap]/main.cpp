#include <iostream>
#include <string.h>

using namespace std;

class Book{
    
public:    
    void setTitle(char * t){
        title = new char[strlen(t) + 1];
        strcpy(title, t);
    }
    
    void afisareBook(){
        cout <<"Carte cu titlul " << title << endl;
    }
    
    char * getTitle(){
        return title;
    }
    
   void getDescription(){
        cout << "Books are pretty okay...";
    }
    
private:
    char * title;

};

int main(){
    
    Book * b1 = new Book();
    // char * infoCarte = b1->afisareBook();
    char * infoCarte = b1->getTitle();
    // char answer = 'y';
    
    cout << b1->getDescription()<<endl;
    char * descriere = b1->getDescription();
    cout << descriere << endl;
}