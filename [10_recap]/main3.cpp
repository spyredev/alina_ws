#include <iostream>
#include <string.h>

using namespace std;


class Matematica{

public:
    static int factorial(int n){
        cout <<n << "! = ";
        int rezultat = 1;
        int i;
        for(i=1; i<=n; i++){
            rezultat = rezultat * i;
        }
        cout << rezultat;
        cout << endl;
        return rezultat;
    }
    
    static int suma(int x, int y){
        // cout <<"Suma dintre " << x<< " si " << y << " este " << x+y <<endl;
        return x+y;
    }
    
    static int suma(int x, int y, int z){
        return x+y+z;
    }
};

int main(){
    
    
    cout <<"==================================================="<<endl;
    // Matematica * mate = new Matematica();
    cout << Matematica::suma(1,2,10) << endl;
    
    int sumaF = Matematica::suma(Matematica::factorial(2), Matematica::factorial(3)); 
    cout << sumaF <<endl;
    
}