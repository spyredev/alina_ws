#include <iostream>

using namespace std;

class Persoana{
  
  public:
    int varsta;
    void afisare();
    
};

void Persoana::afisare(){
    cout <<"Persoana are: " << varsta << " ani"<<endl;
}


int main(){
    
    Persoana p1;
    p1.varsta = 33;
    
    Persoana p2;
    p2.varsta = 44;
    
    p1.afisare();
    p2.afisare();
    
    Persoana::afisare();
    
    return 0;
}