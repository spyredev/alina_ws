#include <iostream>


using namespace std;


class Mate{
  
  
  public: 
    // static inseamna ca nu mai ai nevoie de un obiect
    static void displaySuma(int x, int y){
        cout << "Suma dintre " << x << " si " << y << " este : " << x+y << endl;
    }
    
    static void afiseazaPI(){
        cout <<"PI ESTE: " << 3.1412 << endl;
    }
};


int main(){
    
    // Mate * m = new Mate;
    // m->displaySuma(1,2);
    
    // Mate * m2 = new Mate;
    // m2->displaySuma(1,2);
    
    Mate::displaySuma(1, 2);    
    Mate::afiseazaPI();
    
    return 0;
}