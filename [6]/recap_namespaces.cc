#include <iostream>


namespace alina{
    
    void afisare(){
        std::cout << "O functie care afiseaza..."<< std::endl;
    }   
    
}

namespace util{
    
    void info(){
        std::cout<<"I love pizza"<<std::endl;
    }
    
    void afisare(){
        std::cout <<"Aceasta este alta functie care afiseaza!!!!!!!!!!!!"<<std::endl;
    }
}

using namespace alina;

int main() {
    std::cout << "Hello World!" << std::endl;
    
    char sir[64] = "Hello";
    
    afisare();
    util::info();
    
    util::afisare();
    // cout << sir << endl;
}
