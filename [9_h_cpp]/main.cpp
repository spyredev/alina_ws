#include <iostream>
#include <string.h>

#include "Aplicatie.h"
#include "User.h"

using namespace std;





int main(){
    
    User * u1 = new User();
    u1->setUsername("Rob");
    u1->setPassword("j1234");
    // u1->afisare();
    
    User * u2 = new User();
    u2->setUsername("Halford");
    u2->setPassword("g3333");
    
    
    Aplicatie * app = new Aplicatie();
    app->contNou(u1);
    app->contNou(u2);
    
   
    
    app->contNou(new User("Kim", "kd"));
    app->contNou(new User("x", "1222"));
    
    
   
   // app->contNou(new User());
    // app->utilizatori[4]->setUsername("Glenn"); // utilizatori este private, nu il putem accesa direct
    // app->utilizatori[4]->setPassword("jp238910");
    //app->getUtilizator(4)->setUsername("Glenn");
   // app->getUtilizator(4)->setPassword("jp238910");
    
    app->afisareUtilizatori();
    
    return 0;
}