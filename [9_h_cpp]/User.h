#ifndef USER_H
#define USER_H


class User{
    public:
        void setUsername(char * un);
        void setPassword(char * pwd);
        void setAge(int a);
        char * getUsername();
        char * getPassword();
        int getAge();
        void afisare();
        
        User(){
            
        }
        
        User(char*un, char *pwd){
            setUsername(un);
            setPassword(pwd);
        }
    private:
        char * username;
        char * password;
        int age;
};

#endif