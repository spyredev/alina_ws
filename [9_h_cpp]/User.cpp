#include <iostream>
#include <string.h>

#include "User.h"

using namespace std;


// --- implementare
    void User::setUsername(char * un){
        username = new char[strlen(un)+1];
        strcpy(username, un);
    }
    
    char * User::getUsername(){
        return username;
    }
    
    void User::setPassword(char * pwd){
        password = new char[strlen(pwd)+1];
        strcpy(password, pwd);
    }
    
    char * User::getPassword(){
        return password;
    }
    
    void User::afisare(){
        cout <<"Username: " << username << " password: " << password << endl;
    }
// --- end implementare
