#include <iostream>
#include <string.h>
#include "Aplicatie.h"


using namespace std;



void Aplicatie::contNou(User * u){
    if(validareUtilizator(u)){
        utilizatori[numarUtilizatori++] = u;
    }
}

bool Aplicatie::validareUtilizator(User *u){
    if(strlen(u->getPassword()) < 4){
        return false;
    }
    if(strlen(u->getUsername()) < 3){
        return false;
    }
    return true;
    
}

User * Aplicatie::getUtilizator(int index){
    // if(index > numarUtilizatori)
       // return NULL;
    return utilizatori[index];
}

void Aplicatie::afisareUtilizatori(){
    int i;
    cout <<"In aplicatie sunt utilizatorii---------------------" << endl;
    for(i=0; i<numarUtilizatori; i++){
        utilizatori[i]->afisare();
    }
    cout <<"--------------------------------------------------------" << endl;
}
