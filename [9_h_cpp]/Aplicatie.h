#ifndef APLICATIE_H
#define APLICATIE_H




#include "User.h"




class Aplicatie{
    
    public:
        
        Aplicatie(){
            numarUtilizatori = 0;
        }
        
        void contNou(User * u); // un fel de setter
        User * getUtilizator(int index); // indexul utilizatorului in array-ul utilizatori
        void afisareUtilizatori();
        
        
    private:
        User * utilizatori[100]; // indexul utilizatorului in array-ul utilizatori
        int numarUtilizatori;
        
        bool validareUtilizator(User * usr);
};


#endif