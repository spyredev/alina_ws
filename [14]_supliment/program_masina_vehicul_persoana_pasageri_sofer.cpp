#include <iostream>
#include <string.h>

using namespace std;

class Persoana{
    
public:
    
    int varsta;

    char * getNume(){
        return nume;
    }
    
    void setNume(char * nume){
        this->nume = new char[strlen(nume) + 1];
        strcpy(this->nume, nume);
    }
    
    Persoana(){
        
    }
    
    Persoana(char * nume){
        setNume(nume);
    }
    
private:
    char * nume;
    
};


class Vehicul{
public:
    // Vehicul(){
        
    // }

    Vehicul(int nrRoti, char * numarInmatriculare){
        this->numarInmatriculare = new char[strlen(numarInmatriculare)+1];
        strcpy(this->numarInmatriculare, numarInmatriculare);
        this->numarRoti = nrRoti;
    }
    
    char * getMarca(){
        return this->marca;
    }
    
    void setMarca(char * marca){
        this->marca = new char[strlen(marca)+1];
        strcpy(this->marca, marca);
    }
    
    void afisareVehicul(){
        cout <<"Numar Inmatriculare:"  << this->numarInmatriculare <<" roti: " << this->numarRoti << " Marca: " << this->marca<<endl;
    }
    
    void setSofer(Persoana * pers){
        this->sofer = pers;
        
    }
    
    Persoana * getSofer(){
        return sofer;
    }
    
private:
    char * marca;
    int numarRoti;
    char * numarInmatriculare;
    Persoana * sofer;
};

class Masina : public Vehicul{
public:
    Masina(char * numI) : Vehicul(4, numI){
        numarPasageri = 0;
    }
    
    // Masina(){ ????? CE SE INTAMPLA DACA
    //     numarPasageri = 0;
    // }
    
    void adaugaPasager(Persoana * pers){
        pasageri[numarPasageri] = pers;
        numarPasageri++;
    }
    
    void afiseazaPasageri(){
        int i;
        for(i=0; i<numarPasageri; i++){
            cout << pasageri[i]->getNume() <<" " << pasageri[i]->varsta << endl;
        }
    }
    
private:
    int numarPasageri;
    Persoana * pasageri[8];

};

class Snowmobil{
private:
public:
};

class Motocicleta : public Vehicul{
public:
    Motocicleta(char * param) : Vehicul(2, param){
        
    }
private:

};

int main(){
    
    cout <<"SALUUT"<<endl;
    
    Vehicul * veh1 = new Vehicul(5, "B10DOI");
    veh1->setMarca("Matiz");
    Masina * masina1 = new Masina( "B2ZCE");
    masina1->setMarca("AUDI");
    Masina * masina2 = new Masina( "B33LOL");
    masina2->setMarca("BMW");
    
    masina1->afisareVehicul();
    cout <<"TEST"<<endl;
    veh1->afisareVehicul();
    
    
    
    Motocicleta * m1 = new Motocicleta("8321");
    m1->setMarca("Suzuki");
    
    m1->afisareVehicul();
    
    cout <<"---------------persoane---------------------"<<endl;
    Persoana * pers1 = new Persoana();
    pers1->setNume("Satriani");
    
    Persoana * pers2 = new Persoana();
    pers2->setNume("Steve");
    
    masina1->setSofer(pers1);
    veh1->setSofer(pers2);
    
    
    // cine conduce masina 'masina1':
    cout << "Numele soferului: " << masina1->getSofer()->getNume()<<endl;
    
    cout <<"---------------persoane-2--------------------"<<endl;
    Persoana * pers3 = new Persoana("Ion");
    Persoana * pers4 = new Persoana("Vasile");
    Persoana * pers5 = new Persoana("Glenn");
    
    masina1->adaugaPasager(pers3);
    masina1->adaugaPasager(pers4);
    masina1->adaugaPasager(pers5);
    masina1->afiseazaPasageri();

    cout <<"END"<< endl;
    return 0;
}