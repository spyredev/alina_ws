#include <iostream>
#include <string.h>

using namespace std;

class Cont{
public:

    void setId(int id){
        this->id = new int;
        *(this->id) = id;
    }
    
    int * getId(){
        return this->id;
    }
    
    void setBalance(int balance){
        this->balance = new int;
        *(this->balance) = balance;
        // this->balance = balance;
    }
    
    int * getBalance(){
        return this->balance;
    }

    void setOwner(char * owner){
        this->owner = new char[strlen(owner)+1];
        strcpy(this->owner, owner);
    }
    
    char * getOwner(){
        return this->owner;
    }

    Cont(){
        cout <<"****CONSTRUCTOR SIMPLU****"<< endl;
    }

    Cont(const Cont &altCont){
        
        cout <<"*****CONSTRUCTOR DE COPIERE*****"<<endl;
        this->id = new int;
        *(this->id) = *(altCont.id);
        
        this->balance = new int;
        *this->balance = *altCont.balance;
        
        this->owner = new char[strlen(altCont.owner)+1];
        strcpy(this->owner, altCont.owner);
    }


    void afisareCont(){
        cout <<"["<<*id<<"] - " << owner<<" - " << *balance <<endl;
    }

private:


    int * id;
    int * balance;
    char * owner;
};



int main(){
    
    Cont * c1 = new Cont();
    c1->setBalance(3000);
    c1->setId(432109);
    c1->setOwner("Jim");
    c1->afisareCont();
    
    Cont *  c2 = new Cont(*c1);
    
    // c1->setId(-20);
    
    c2->afisareCont();
    
    cout <<"======================================"<<endl;
    Cont c3 = *c1;
    
    
    c3.afisareCont();
    
    Cont * contNou = c1;
    return 0;
}












/*
// void afisareCont(const Cont & c){
//     cout <<"AFISARE CONT: ID = " << *c.id << endl;
// }

void modificare(int & p){
    p = 3000;
}

int main200(){
    
    
    Cont c1;
    int * id = new int;
    *id = 321;
    c1.setId(id);
    cout << "ID-ul contului este: " << *c1.getId()<<endl;
    
    *id = 28;
    
    
    cout << "ID-ul contului este: " << *c1.getId()<<endl;
        
    afisareCont(c1);
    
    int q = 2;
    modificare(q);
    cout << " q = " << q << endl;
    
    return 0;
}*/