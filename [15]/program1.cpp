#include <iostream>
#include <string.h>

using namespace std;

class Carte{
public:
    double pret;
    
    friend void afisareCarte();
    
    void afisareCarte2(){  // obiect.afisareCarte2();
        cout <<"Pretul cartii este: " << pret << endl;
    }
    
    void afisareCarte3();  // obiect.afisareCarte3();
private:
};


void Carte::afisareCarte3(){
    cout <<"Pretul cartii este: " << pret << endl;
}

void functie1(int &x){
    x = x+100;
}

void afisareCarte(Carte carte){
    cout << "Pretul este: " << carte.pret << endl;
}


int main(){
    
    Carte c1;
    c1.pret = 30.22;
    Carte c2;
    c2.pret = 44.10;
    
    c1.afisareCarte2();
    c2.afisareCarte2();
    
    int var = 30;
    functie1(var);
    cout <<"Var: " << var << endl;
    
    afisareCarte(c1);
    
    return 0;
}