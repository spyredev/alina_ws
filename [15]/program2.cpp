#include <iostream>
#include <string.h>

using namespace std;

class Carte{
public:
    double pret;

    Carte(){
        
    }
    
    Carte(char * titlu, double pret){
        this->titlu = new char[strlen(titlu)+1];
        strcpy(this->titlu, titlu);
        this->pret = pret;
    }
    
    Carte(Carte * altaCarte){
        // this->titlu = altaCarte->titlu; // titlu din c1 si c2 / this, altaCarte va fi acelasi
        this->titlu = new char[strlen(altaCarte->titlu)+1];
        strcpy(this->titlu, altaCarte->titlu);
        this->pret = altaCarte->pret;
    }

    
    void afisare(){  // obiect.afisareCarte2();
        cout <<"Pretul cartii este: " << pret << " si titlul: " << titlu << endl;
    }
    
    
    void setTitlu(char * titlu){
        this->titlu = new char[strlen(titlu)+1];
        strcpy(this->titlu, titlu);
    }
    
    char * titlu;
    
private:
    
};


int main(){
    
    Carte c1("Pandora", 99.22);
    c1.afisare();
    
    
    
    Carte c2(&c1);
   
    cout <<"------"<<endl; 
    c2.afisare();
    strcpy(c1.titlu, "C1 - Gods Themselves");
    c1.pret = 110.10;
    c2.afisare();
    
    
    return 0;
}