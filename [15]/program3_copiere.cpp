#include <iostream>
#include <string.h>

using namespace std;


class Produs{
public:
    double * pret;
    
    Produs(double p){
        this->pret = new double;
        *(this->pret) = p;
    }
    
    Produs(Produs * altProdus){
        this->pret = new double;
        *(this->pret) = *(altProdus->pret);
    }
    
    Produs(){}
    
private: 
};

int main(){
    
    Produs p1;
    p1.pret = new double;
    *(p1.pret) = 44.0;
    cout << *(p1.pret) << endl;
    
    Produs p2(17.89);
    
    Produs p3(&p2);
    
    *(p2.pret) = 77.77;
    
    cout <<"Pret p2: " << *(p2.pret) << endl;
    cout <<"Pret p3: " << *(p3.pret) << endl;
    
    return 0;
}