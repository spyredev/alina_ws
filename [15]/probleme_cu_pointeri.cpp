#include <iostream>
#include <string.h>

using namespace std;



int main(){
    
    int numar = 30;
    int * pointerNumar = &numar;
    
    // numar = 40;
    *pointerNumar = 40;
    
    cout <<"Numarul este: " << numar << endl;
    cout <<"Pointerul este: " << pointerNumar << endl;
    cout <<"Valoarea pointerului este: " << *pointerNumar << endl;
    
    cout <<"==========================================================="<<endl;
    
    // int * p1;
    // p1 = new int;
    int * p1 = new int;
    *p1 = 4000;
    
    cout <<"Val: " << *p1<<endl;
    
    int * p2 = p1;
    *p2 = 5000;
    
    cout <<"Val: " << *p1<<endl;
    
    cout <<"==========================================================="<<endl;

    int * pointerA = new int;
    *pointerA = 87;

    int * pointerB = new int;
    *pointerB = 1000;
    *pointerA = *pointerB;
    
    *pointerB = 4;

    cout << *pointerA << endl;
     
    
    return 0;
}