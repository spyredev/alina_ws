#include <iostream>
#include <string.h>

using namespace std;

class Mancare{
public:
    Mancare(const Mancare & m){
        cout <<"**COPIERE"<<endl;
        this->denumire = new char[strlen(m.denumire)+1];
        strcpy(this->denumire, m.denumire);
    }
    
    Mancare(){
        cout <<"**STANDARD"<<endl;
    }
    
    Mancare(char * denumire){
        cout <<"**1PARAM"<<endl;
        this->setDenumire(denumire);
    }
    
    void setDenumire(char * denumire){
        this->denumire = new char[strlen(denumire)+1];
        strcpy(this->denumire, denumire);
    }
    
    char * getDenumire(){
        return this->denumire;
    }

    void afisare(){
        cout <<"Denumire: " << denumire << endl;
    }
    
private:
    char * denumire;
    
};


int main(){
    
    Mancare * manc1 = new Mancare();
    manc1->setDenumire("Prajitura");
    manc1->afisare();
    
    
    // Mancare * manc2 = manc1;  // ACeLASI LUCRU
    
    Mancare manc2 = *manc1; // Mancare manc2 = new Mancare(*manc1);
    
    manc1->setDenumire("PIZZA");
    
    manc2.afisare();
    
    cout << "---------------------------" << endl;
    
    Mancare m("Ciocolata");
    Mancare m2 = m; // COPIERE
    m.setDenumire("Chipsuri cu ciocolata");
    
    m2.afisare();
    
    
    cout << "---------------------------" << endl;
    
    Mancare * mancare1 = new Mancare("Pringles");
    Mancare mancare2 = *mancare1; // COPIE
    Mancare * mancare3 = new Mancare("Lays");
    
    mancare2.setDenumire("Kek");
    
    
    mancare1->afisare();
    
    return 0;
}