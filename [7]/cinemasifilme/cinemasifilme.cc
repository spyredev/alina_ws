#include <iostream>
#include <string.h>

using namespace std;

/// TODO:
///     Film
///         gen, length (127 minute), title
///     un program care ne permite sa adaugam filme
///         cu modificari minime
///         - stergem filme
///         - afisam filmele curente
///     Cinema

class Film
{
public:
    int lungime;

    void setTitlu(char * t){
        titlu = new char[strlen(t)+1];
        strcpy(titlu, t);
    }

    char * getTitlu(){
        return titlu;
    }

    void setGen(char * g){
        gen = new char[strlen(g)+1];
        strcpy(gen, g);
    }

    char * getGen(){
        return gen;
    }

    Film(char * t, int l, char * g){
        titlu = new char[strlen(t)+1];
        strcpy(titlu, t);
        lungime = l;
        gen = new char[strlen(g)+1];
        strcpy(gen, g);
    }


private:
    char * titlu;
    char * gen;
};

  /// TODO: o metoda / functie care afiseaza filmele in ordinea duratei

// diferite functii pe care le punem in FilmeUtil ca sa nu aglomeram 'Cinema'
class FilmeUtil{
  public:
    static void sortareFilmeDupaDurata(Film * filme[], int numarFilmeCeTrebuieSortate){
        int i, j;
        for(i=0; i<numarFilmeCeTrebuieSortate; i++){
            for(j=0; j<numarFilmeCeTrebuieSortate; j++){
                if(filme[i]->lungime < filme[j]->lungime){
                    Film *  aux;
                    aux = filme[i];
                    filme[i] = filme[j];
                    filme[j] = aux;
                }
            }
        }
    }

    static void afisareFilme(Film * filme[], int nrFilme){
        int i;
        for(i=0; i<nrFilme; i++){
            cout << "TITLU: " << filme[i]->getTitlu() <<", " << filme[i]->getGen() <<" durata: " << filme[i]->lungime << endl;
        }
    }

};

class Cinema{
public:
    Film * filmeLaCinema[100];

    void afisareFilmeLaCinema(){
        for(int i=0; i<nrFilme; i++){
            cout<<"La cinema este filmul: "<<filmeLaCinema[i]->getTitlu()<<" si este "<<filmeLaCinema[i]->getGen()<<endl;
        }
    }

    void adaugareFilm(Film * f){
        filmeLaCinema[nrFilme] = f;
        nrFilme++;
    }

    // sterge de la sfarsit
    void stergereFilm(char * titlu){
        // filmeLaCinema[nrFilme] = f;

        int i;
        int pozitiaFilmuluiPeCareVremSa_lStergem = -1;
        for(i=0; i<nrFilme; i++){
            if(strcmp(filmeLaCinema[i]->getTitlu(), titlu) == 0){
                // cout <<"Am gasit filmul pe care vrem sa-l stergem: " << titlu << endl;
                pozitiaFilmuluiPeCareVremSa_lStergem = i; // variabila pozitie o sa ia index-ul (i) care reprezinta pozitia din vector de unde
                            // trebuie sa stergem elementul
            }
        }

        if(pozitiaFilmuluiPeCareVremSa_lStergem != -1){  // daca am gasit o pozitie valida
            // cout <<"Am gasit filmul, trebuie sa stergem elementul de la pozitia: " << pozitie << endl;
            stergereFilm(pozitiaFilmuluiPeCareVremSa_lStergem); // stergem filmul de la pozitia i (pozitie)
            // nrFilme--; // nu mai este necesar, pentru ca stergereFilm(int) oricum micsoreaza numarul de filme din cinema
        }

    }

    // putem sterge orice film de la primul la ultimul (????)
    void stergereFilm(int pozitie){

        if(pozitie >= nrFilme){
            cout << "Nu pot sterge elementul de pe pozitia: " << pozitie << endl;
        }else{

            int i;
            for(i=pozitie; i<nrFilme-1; i++){
                filmeLaCinema[i] = filmeLaCinema[i+1];
            }
            nrFilme--;
        }
    }

    Cinema(){
        nrFilme = 0;
    }

private:
    int nrFilme;
};


int main()
{
    Cinema cin;

    Film f1("School of rock", 116, "comedie");

    Film * f2;
    f2 = new Film("Need for speed", 127, "actiune");

    Film * f3 = new Film("Real Steel", 108, "SF");

    Film f4("Star Wars", 160, "SF");
    Film f5("Fast and Furious", 120, "actiune");

    cin.adaugareFilm(&f1);
    cin.adaugareFilm(f2);

    // cin.stergereFilm(f2);

    cin.adaugareFilm(f3);
    cin.adaugareFilm(&f4);
    cin.adaugareFilm(&f5);

    cin.afisareFilmeLaCinema();
    cout <<"Dupa stergere: " <<endl;
    // cin.stergereFilm(222);
    cin.stergereFilm("Star Wars");

    cin.afisareFilmeLaCinema();

    cout <<"================================================\n\n";

    int i;
    for(i=0; i<  4; i++){
        cout << cin.filmeLaCinema[i]->getTitlu() << endl;
    }

    cout <<"=======================ORDONARE FILME DURATA=========================\n\n";


    Film * films[10];
    films[0] = &f1;
    films[1] = f2;
    films[2] = f3;
    films[3] = &f4;
    films[4] = &f5;

    // FilmeUtil * obiect;
    // obiect->afisareFilme(films, 5);
    // obiect->sortareFilmeDupaDurata(films, 5);
    cout <<"-------------\n";
    // obiect->afisareFilme(films, 5);

    // FilmeUtil obiect2;
    // obiect2.afisareFilme(films, 5);

    FilmeUtil::afisareFilme(films, 5);
    cout <<"-------------\n";
    FilmeUtil::sortareFilmeDupaDurata(films, 5);
    FilmeUtil::afisareFilme(films, 5);
    cout <<"-------------\n";

    return 0;
}
