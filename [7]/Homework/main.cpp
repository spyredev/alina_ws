#include <iostream>
#include <string.h>

using namespace std;

/// ce metode sunt statice :)
/* Student
    - nume, email, note
    - addNota
    - addTask
StudentUtil
    - metoda care primeste un student si afiseaza media lui
        double calculeazaMedie(Student * s){
            // return
        }

Fiecare student are Task-uri
    un task poate fi completat (true/false)
    - are un titlu
    - descriere
    
    
Un student poate renunta la un task 
    

    - [[[fiecare task are o data la care a fost adaugat si o data la care trebuie sa se termine]]]

TaskUtil
    - primeste un task si zice daca s-a terminat in timp util sau daca mai e timp sa-l terminam
*/


class Task{
    public:
        void setFinished(bool f){
            finished = f;
        }
        
        bool getFinished(){
            return finished;
        }
        
        void setTitlu(char * t){
            titlu = new char[strlen(t)+1];
            strcpy(titlu, t);
        }
        
        void setDescriere(char * d){
            descriere = new char[strlen(d)+1];
            strcpy(descriere, d);
        }
        
        Task(char * t, char * d){
            descriere = new char[strlen(d)+1];
            strcpy(descriere, d);
            titlu = new char[strlen(t)+1];
            strcpy(titlu, t);
        }
        
        void afisare();
        
    private:
        bool finished;
        char * titlu;
        char * descriere;
};


void Task::afisare(){
    cout <<"Titlu: "<< titlu <<" [" << descriere <<"] done: " << finished << endl;
        
}






class Student
{
public:


    
   
    
    void addTask(Task * t){
        taskuri[numarTaskuri++] = t;
    }
    
    void renuntaLaTask(char * numeTask);
    void renuntaLaTask(Task * t);
    
    // end tasks

    int note[100];
    char email[100];

    Student(char * n, char * e)
    {
        numarCurentNote = 0;
        numarTaskuri = 0;
        strcpy(nume, n);
        strcpy(email, e);
    }

    Student()
    {
        numarCurentNote = 0;
    }

    void setNume(char * n)
    {
        strcpy(nume, n);
    }

    char * getNume()
    {
        return nume;
    }

    void addNota(int nota)
    {
        cout <<"Studentul "<< getNume() <<" are: " << numarCurentNote <<" note"<< " si adaugam la pozitia urmatoare nota: " << nota <<endl;
        note[numarCurentNote] = nota;
        numarCurentNote++;
    }

    void afiseazaNote()
    {
        cout <<"Notele lui "<< nume << " sunt: ";
        for(int i=0; i<numarCurentNote; i++)
            cout << note[i] <<", ";
            cout << endl;
    }

    int getNumarCurentNote(){
        return numarCurentNote;
    }

    /*
    double getMedie(){
        double sumaNote = 0;
        for(int i=0; i<numarCurentNote; i++){
            sumaNote += note[i];
        }
        
        return  sumaNote / numarCurentNote;
    }*/

private:
    char nume[50];
    int numarCurentNote;
    int numarTaskuri;
    

    Task * taskuri[100];
};


void Student::renuntaLaTask(char * numeTask){
    // modifici array-ul vectorul 
}
void Student::renuntaLaTask(Task * t){
    
}
    


class StudentUtil{
  public:
  
    static double calculeazaMediePentruUnStudent(Student * s){
        double medie = 0;
        int i;
        for(i=0; i<s->getNumarCurentNote(); i++){
            medie += s->note[i];  // note este public in Student
            
        }
        medie = medie / s->getNumarCurentNote();
        return medie;
    }
  
     static double calculeazaMedie(Student * studenti[], int nrStudenti) // static double calculeazaMedie(Clasa * cl) 
    {
        
        double medieTotala = 0;
        int i;
        for(i=0; i<nrStudenti; i++){
            medieTotala += calculeazaMediePentruUnStudent(studenti[i]); 
            // medieTotala += studenti[i]->getMedie();
        }
        
        medieTotala = medieTotala / nrStudenti;
        return medieTotala;
        /*
        if(numarCurentNote == 0)
        {
            return 0;
        }
        else
        {
            double medieFinala = 0;
            for(int i=0; i<numarCurentNote; i++)
            {
                medieFinala += note[i];
            }
            medieFinala = medieFinala / numarCurentNote;
            return medieFinala;
        }
        */
    }
};

int main()
{
    Student * s1;
    s1=new Student("Bogdan","bogdan_91@yahoo.com");
    Student s2("Edi","eduard27@gmail.com");

    s1->addNota(9);
    s1->addNota(10);

    cout<<"----------------------------------------------------"<<endl;

    s2.addNota(7);
    s2.addNota(8);
    s2.addNota(9);

    cout<<"----------------------------------------------------"<<endl;

    s1->afiseazaNote();
    s2.afiseazaNote();

    Student * studenti[10];
    studenti[0] = s1;
    studenti[1] = &s2;
    studenti[2] = new Student("Bob", "bobby@gmail.com");
    studenti[2]->addNota(10);
    studenti[2]->addNota(9);
    studenti[2]->addNota(7);
    
    studenti[2]->afiseazaNote();

    cout << "----------------------------------------------------"<<endl;

    cout <<"Media este : " << StudentUtil::calculeazaMedie(studenti, 3)<<endl;
    
    if(StudentUtil::calculeazaMediePentruUnStudent(s1) < StudentUtil::calculeazaMedie(studenti, 3)){
        cout << s1->getNume() << " are media sub media clasei"<<endl;
    }else{
        cout << s1->getNume() << " are media peste media clasei"<<endl;
    }

    cout << "----------------------Tasks------------------------------"<<endl;
    Task * t1 = new Task("buy pizza", "Buy pizza from Jerry's pizza");
    Task * t2 = new Task("drink coke", "Drink some coke to replace that nasty coffee");
    Task * t3 = new Task("read book", "Read some interesting book");
    
    t1->afisare();
    t2->afisare();
    t3->afisare();
    
    // studentii au task-uri
    
    // s1->taskuri[0] = t1;
    s1->addTask(t1);
    // s1->taskuri[1] = t2;
    s1->addTask(t2);
    
    // s2.taskuri[0] = t3;
    s2.addTask(t3);
    
    s1->renuntaLaTask("drink coke");
    // s1->renuntaLaTask(t2);
    
    return 0;
}
