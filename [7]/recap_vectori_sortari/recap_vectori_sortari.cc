#include <iostream>

using namespace std;

void afisare(int unVector[], int lungimeaLui){
    int i;
    cout <<"[";
    for(i=0; i<lungimeaLui; i++){
        cout << unVector[i] <<", ";
    }
    cout <<"]"<<endl;
}

void sortare(int v[], int lung){
    int i,j;
    for(i=0; i<lung; i++){
        // cout << v[i] <<" " << endl;
        for(j=0; j<lung; j++){
            //cout << v[j] << ", ";

            if(v[i] < v[j]){
                int aux;
                aux = v[i];
                v[i] = v[j];
                v[j] = aux;
            }
        }
    }
}

int main()
{
    int v[10] = {3,1,4,10,2,5,7,1,8,22};

    afisare(v, 10);
    sortare(v, 10);
    afisare(v, 10);

    return 0;
}
