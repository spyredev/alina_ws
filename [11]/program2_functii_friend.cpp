#include <iostream>
#include <string.h>

using namespace std;

// Persoana
//      nume, prenume, varsta
        // getNume, setNume, getPrenume, setPrenume, get,set Varsta
//      emails - 0, 1, 10, 173, ...

class Persoana{
public:

    Persoana(char * nume, char * prenume){
        this->nume = new char[strlen(nume)+1];
        this->prenume = new char[strlen(prenume)+1];
        strcpy(this->nume, nume);
        strcpy(this->prenume, prenume);
        
    }
    
    void setVarsta(int v){
        this->varsta = v;
    }
    
    friend void afisarePersoana(Persoana *p);
    
private:
    char * nume;
    char * prenume;
    int varsta;
};


// void Persoana::afisarePersoana(Persoana * p){
//     cout <<"Nume: " << p->nume << " prenume: " << p->prenume <<" varsta: " << p->varsta << endl;
// }

void afisarePersoana(Persoana * p){
    cout <<"Nume: " << p->nume << " prenume: " << p->prenume <<" varsta: " << p->varsta << endl;
}


int main(){
    
    Persoana * pers = new Persoana("Bale", "Christian");
    pers->setVarsta(42);
    
    Persoana * bullshit = new Persoana("Lolescu", "Lol");
    // Persoana::afisarePersoana(pers);
    // Persoana::afisarePersoana(bullshit);
    
    afisarePersoana(pers);
    
    return 0;
}