#include <iostream>
#include <string.h>

using namespace std;

// Persoana
//      nume, prenume, varsta
        // getNume, setNume, getPrenume, setPrenume, get,set Varsta
//      emails - 0, 1, 10, 173, ...

class Persoana{
public:

    Persoana(char * nume, char * prenume){
        this->nume = new char[strlen(nume)+1];
        this->prenume = new char[strlen(prenume)+1];
        strcpy(this->nume, nume);
        strcpy(this->prenume, prenume);
        nrEmails = 0;
    }
    
    int getVarsta(){
        return varsta;
    }
    
    void setVarsta(int pvarsta){
        this->varsta = pvarsta;
    }
    
    void setPrenume(char * pn){
        prenume = new char[strlen(pn)+1];
        strcpy(prenume, pn);
    }
    
    char * getPrenume(){
        return prenume;
    }
    
    // void setEmails(char * emails[100]){
        // this->emails = emails;
    // }
    
    void addEmail(char * email){ // setter 'avansat'
        emails[nrEmails] = new char[strlen(email)+1];
        strcpy(emails[nrEmails++], email);
        // nrEmails++;
    }
    
    char * getEmail(int pozitie){
        return emails[pozitie];
    }
    
    void afisareEmailuri(); // in clasa un rand
    
    
    
    
private:
    char * nume;
    char * prenume;
    int varsta;
    
    
    char * emails[100];
    int nrEmails;
    
}; // END Persoana


// :: folosit ca sa definesti in exteriorul clasei
void Persoana::afisareEmailuri(){  // in afara clasei definitia functiei afisareEmailuri()
        cout <<nume << " are mail-urile: " <<endl;
        int i;
        for(i=0; i<nrEmails; i++){
            cout <<"\t\t" << emails[i] << endl;
        }
}
    


class Text{
public:
    static void afisareSeparator(){
        cout <<"========================================================================================"<<endl;
    }
    
private:
};


int main(){
    
    // Text text;
    // Text * altText = new Text; // PENTRU METODELE STATICE NU AI NEVOIE DE OBIECTE
    // Text::afisareSeparator(); // :: pentru apelare metode statice
    
    Persoana p1("Ionescu", "Ion");
    // p1.varsta = 23;
    p1.setVarsta(23);
    cout << p1.getVarsta() << endl;
    
    Persoana *p2 = new Persoana("Vasilescu", "Vasile");
    cout << p2->getPrenume()<<endl;
    
    p2->setVarsta(44);
    p2->setPrenume("Jim");
    cout <<p2->getVarsta() << endl;
    
    
    cout << p2->getPrenume() << endl;
    // char * mailurip2[100];
    // mailurip2[0] = new char[30];
    // strcpy(mailurip2[0], "vasile@gmail.com");
    // mailurip2[1] = new char[32];
    // strcpy(mailurip2[1], "vasile@yahoo.com");
    
    // vasile@gmail.com, vasile@yahoo.com
    
    // p2->setEmails(mailurip2);
    p2->addEmail("vasile@gmail.com");
    p2->addEmail("vasile@yahoo.com");
    p2->afisareEmailuri();
    
    Text::afisareSeparator();
    
    // p2 - vasile@gmail.com, vasile@yahoo.com
    
    char em[64];
    strcpy(em, "test@google.com");
    cout << strstr(em, "@") << endl;
    
    
    
    Text::afisareSeparator();
    cout << p2->getEmail(0) << endl;
    cout << p2->getEmail(1) << endl;
    
    cout << strstr(p2->getEmail(0), "@") << endl;
    
    Text::afisareSeparator();
     
    return 0;
}