#include <iostream>
#include <string.h>

using namespace std;

class User{
    public:
        void setUsername(char * un);
        void setPassword(char * pwd);
        void setAge(int a);
        char * getUsername();
        char * getPassword();
        int getAge();
        void afisare();
        
        User(){
            
        }
        
        User(char*un, char *pwd){
            setUsername(un);
            setPassword(pwd);
        }
    private:
        char * username;
        char * password;
        int age;
};

// --- implementare
    void User::setUsername(char * un){
        username = new char[strlen(un)+1];
        strcpy(username, un);
    }
    
    char * User::getUsername(){
        return username;
    }
    
    void User::setPassword(char * pwd){
        password = new char[strlen(pwd)+1];
        strcpy(password, pwd);
    }
    
    char * User::getPassword(){
        return password;
    }
    
    void User::afisare(){
        cout <<"Username: " << username << " password: " << password << endl;
    }
// --- end implementare

class Aplicatie{
    
    public:
        
        Aplicatie(){
            numarUtilizatori = 0;
        }
        
        void contNou(User * u); // un fel de setter
        User * getUtilizator(int index); // indexul utilizatorului in array-ul utilizatori
        void afisareUtilizatori();
        
        
    private:
        User * utilizatori[100]; // indexul utilizatorului in array-ul utilizatori
        int numarUtilizatori;
        
        bool validareUtilizator(User * usr);
};

void Aplicatie::contNou(User * u){
    if(validareUtilizator(u)){
        utilizatori[numarUtilizatori++] = u;
    }
}

bool Aplicatie::validareUtilizator(User *u){
    if(strlen(u->getPassword()) < 4){
        return false;
    }
    if(strlen(u->getUsername()) < 3){
        return false;
    }
    return true;
    
}

User * Aplicatie::getUtilizator(int index){
    // if(index > numarUtilizatori)
       // return NULL;
    return utilizatori[index];
}

void Aplicatie::afisareUtilizatori(){
    int i;
    cout <<"In aplicatie sunt utilizatorii---------------------" << endl;
    for(i=0; i<numarUtilizatori; i++){
        utilizatori[i]->afisare();
    }
    cout <<"--------------------------------------------------------" << endl;
}


int main(){
    
    User * u1 = new User();
    u1->setUsername("Rob");
    u1->setPassword("j1234");
    // u1->afisare();
    
    User * u2 = new User();
    u2->setUsername("Halford");
    u2->setPassword("g3333");
    
    
    Aplicatie * app = new Aplicatie();
    app->contNou(u1);
    app->contNou(u2);
    
   
    
    app->contNou(new User("Kim", "kd"));
    app->contNou(new User("x", "1222"));
    
    
   
   // app->contNou(new User());
    // app->utilizatori[4]->setUsername("Glenn"); // utilizatori este private, nu il putem accesa direct
    // app->utilizatori[4]->setPassword("jp238910");
    //app->getUtilizator(4)->setUsername("Glenn");
   // app->getUtilizator(4)->setPassword("jp238910");
    
    app->afisareUtilizatori();
    
    return 0;
}