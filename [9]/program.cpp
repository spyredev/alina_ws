#include <iostream>

using namespace std;


// prototip
class Stuff{

    
public:
    int varsta;

    void m1();
    void m2();
    void m3();
    void m4();

    
    void afisare();
    
    
    static void testStatic(){
        cout <<"METODA ASTA ESTE STATICA!!!"<<endl;
    }
    
    void interfata(){
        testPrivate();
    }
private:   
    void testPrivate();
};

// implementare metode din prototip ---------------------- START
void Stuff::m1(){
        cout <<"This is method 1"<<endl;
}

void Stuff::m2(){
    cout <<"This is method 2"<<endl;
}

void Stuff::m3(){
    
}

void Stuff::m4(){
    cout <<"This is method 4" <<endl;
}
    
void Stuff::afisare(){
    cout << "Varsta este: " << varsta << endl;
}

void Stuff::testPrivate(){
    cout <<"HOLA"<<endl;
}
// implementare metode din prototip ---------------------- END

int main(){
    
    Stuff::testStatic();
    Stuff s1;
    s1.m1();
    s1.testPrivate();
    return 0;
}