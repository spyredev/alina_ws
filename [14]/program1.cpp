#include <iostream>
#include <string.h>

using namespace std;

class Elev{
public:
    void setNume(char * nume){
        this->nume = new char[strlen(nume)+1];
        strcpy(this->nume, nume);
    }
    
    Elev(char * nume){
        this->nume = new char[strlen(nume)+1];
        strcpy(this->nume, nume);
    }
    
    Elev(){
        
    }
private:
    char * nume;
    
};

int main(){
    
    Elev e1;
    Elev * e2 = new Elev("Elev");
    e2->setNume("George");
    
    cout <<"END"<<endl;
    
    Elev * maiMulti[100];
    maiMulti[0] = &e1;
    maiMulti[1] =  e2;
    
    
    int numere[5] = {2,10};     // 2   10   x   x    x
    int numere[] = {1,3}; // 1  3 ......
    return 0;
}