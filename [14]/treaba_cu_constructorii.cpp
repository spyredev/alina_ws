#include <iostream>
#include <string.h>

using namespace std;

class Reteta{
public:
    
    Reteta(char * nr){
        this->numereteta = new char[strlen(nr)+1];
        strcpy(this->numereteta, nr);
    }
    
    Reteta(){
        
    }
    
    char * getNumereteta(){
        return numereteta;
    }
    
private:
char * numereteta;
};

class RetetaCompensata : public Reteta{
public:
    RetetaCompensata(float procentCompensat, char * numeReteta) : Reteta(numeReteta){
        this->procentCompensat = procentCompensat;
    }
    
    RetetaCompensata(float procentCompensat){
        this->procentCompensat = procentCompensat;
    }
private:

    float procentCompensat;
};

int main(){
    
    Reteta r1("Reteta pentru raceala");
    cout << r1.getNumereteta() << endl;
    
    RetetaCompensata r2(0.5f, "Reteta pentru dureri spate");
    
    cout << r2.getNumereteta() << endl;
    
    RetetaCompensata r3(0.7f, "Reteta pentru dureri cap");
    cout << r3.getNumereteta() << endl;
    
    RetetaCompensata r4(9.2f);
    
    cout <<"END"<<endl;
}