#include <iostream>
#include <string.h>

using namespace std;

class Baza{
  public:
    virtual void test(){
        cout <<"HELLO"<<endl;
    }
};

class Derivata : public Baza{
public:

    virtual void test(){
        Baza::test();
    }

private:
    
};

int tva(){
    return 24;
}

void afisareTva(){
    cout << "TVA: " << 24<< endl;
}

int main(){
    Derivata d;
    d.test();
    
    // afisareTva();
    cout << tva();
    
    return 0;
}