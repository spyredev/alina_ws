#include <iostream>
#include <string.h>
#include <fstream>

using namespace std;

class Produs{

public:
    void setPret(double pret){
        this->pret = pret;
    }
    
    double getPret(){
        return pret;
    }
    
    void setCantitate(int cantitate){
        this->cantitate = cantitate;
    }
    
    int getCantitate(){
        return this->cantitate;
    }
    
    void setDenumire(char * denumire){
        this->denumire = new char[strlen(denumire)+1];
        strcpy(this->denumire, denumire);
    }
    
    char * getDenumire(){
        
        return denumire;
    }
    
    Produs(char * denumire, double pret){
        setDenumire(denumire);
        this->pret = pret;
        this->cantitate = 0;
    }
    
    
     Produs& operator++( int )         
      {
        this->cantitate++; // modificam obiectu curent (this)
        return *this;
      }
      
      
     Produs& operator+= (int cantitate)  // Implement +=
      {
        this->cantitate += cantitate;
        return *this;
      }
    

   friend ostream &operator<<( ostream &output, Produs &produs )
      { 
         output << "Denumire : " << produs.getDenumire() << " pret: " << produs.getPret() << " cantitate: " << produs.getCantitate() << endl; 
         return output;            
      }


       bool operator <(const Produs& p2)
      {
         double valoareCurenta = this->cantitate * this->pret;
         double valoareCelalaltProdus = p2.cantitate * p2.pret; 
         
         if(valoareCurenta < valoareCelalaltProdus)
            return true;
         if(valoareCurenta > valoareCelalaltProdus)
            return false;
         if (valoareCurenta == valoareCelalaltProdus)
            return false;
        return false;
      }
    
        virtual double GetValoare(){
            return pret * cantitate;
        }
    
private:
    
    char * denumire;
    double pret;
    int cantitate;  
protected:

};


class ProdusDiscount : public Produs{
public:
    
    void setValoareDiscount(double valoareDiscount){
        this->valoareDiscount = valoareDiscount;
    }
    
    double getValoareDiscount(){
        return this->valoareDiscount;
    }
    
    ProdusDiscount(char * denumire, double pret, double discount) : Produs(denumire, pret){
        this->valoareDiscount = discount;
    }
    
    virtual double GetValoare(){
        return Produs::GetValoare() - this->getPret() * this->getCantitate() * (this->valoareDiscount/100);
    }
    
private:
    double valoareDiscount;
};

class Cos{

public:

    Cos(){
        this->numarProduse = 0;
    }

    Cos(Produs ** produse, int n){
        
        this->numarProduse = n;
        this->produse = produse;
    }
    

    double Total(){
        cout <<"Afisare TOTAL: "<<numarProduse<<endl;
        int i;
        double valoareTotala = 0;
        for(i=0; i<numarProduse; i++){
            // cout << *produse[i];
            valoareTotala += (*produse[i]).GetValoare();
        }
        return valoareTotala;
    }
    
    void afisareProduseCos(){
        // cout << *produse << endl;
        int i;
        for(i=0; i<numarProduse; i++){
            cout << *produse[i];
        }
    }
      
      Cos& operator+= (Produs * prod) 
      {
        produse[numarProduse] = prod;
        numarProduse++;
        return *this;
      }
      
      void scriereInFisier(ofstream fisier){
          fisier <<"Continutul cosului: " << endl;
          int i;
          for(i=0; i<numarProduse; i++){
              fisier << produse[i]->getDenumire() << " - " << produse[i]->getPret()<<endl;
              //fisier << *produse[i];
          }
          
          
      }
private:
    int numarProduse;
    Produs ** produse;
    
};

int main(){
    
    Produs mere("Mere", 4.1);
    Produs pere("Pere", 6.5);
    
    mere++;
    pere++;
    cout << mere.getCantitate() << endl;
    cout << pere.getCantitate() << endl;
    
   //  mere + 5; // mere = mere + 5;
    mere += 5;
    cout << mere.getCantitate() << endl;
    
    cout << mere;
    cout << pere++;
    cout << pere;
    if(mere < pere){
        cout <<"Valoare merelor este mai mica" << endl;
    }else{
        cout <<"Valoare perelor este mai mica"<< endl;
    }
    
    cout <<" ----------------------------- " << endl;

    ProdusDiscount prodDis("Pizza", 9.09, 50);
    // prodDis.setValoareDiscount(0.5);
    ProdusDiscount struguri("Struguri", 8, 15);
    struguri += 10;
    cout << struguri;
    cout << struguri.GetValoare() << endl;
    
    cout <<" ----------------------------- " << endl;
    
  
    int numarProduse = 200;
    Produs ** produse = new Produs*[numarProduse];
    int i;
    produse[0] = new Produs("Pasta", 33.22);
    (*produse[0])++;
    (*produse[0])++;
    produse[1] = new Produs("Calzzone", 17.99);
    produse[2] = new Produs("Pepperoni", 29.99);

    
    
    
    cout <<" ----------------------------- " << endl;
    cout <<" ----------------------------- " << endl;
    cout <<" ----------------------------- " << endl;
    
    Cos c1(produse, 3);
    
    
    
    c1 += new Produs("Tastatura wireless", 44.66);
    
    Produs *unProd = new Produs("Mouse", 12.23);
    
    c1 += unProd;
    c1 += &mere;
    c1 += &pere;
    
    c1.afisareProduseCos();
    cout << c1.Total();
    
    cout << endl;
    cout <<"=========================================================" << endl;
    ofstream fisier("date_cos.txt");
    c1.scriereInFisier(fisier);
   
    return 0;
}