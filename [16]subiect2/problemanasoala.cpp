#include <iostream>
#include <string.h>

using namespace std;

class Persoana
{
public:
    int varsta;
    
    
    // cout << obj << endl;
    friend ostream &operator <<(ostream &output, const Persoana & pers)
    {
        output <<"Sunt o persoana si am " << pers.varsta << " ani." << endl;
        return output;
    }
private:
    
};

int main(){
    
    Persoana * p1 = new Persoana();
    p1->varsta = 22;
    
    Persoana * p2 = new Persoana();
    p2->varsta = 23;
    
    Persoana * p3 = new Persoana();
    p3->varsta = 24;
    
    Persoana ** persoane = new Persoana* [3];
    persoane[0] = p1;
    persoane[1] = p2;
    persoane[2] = p3;
    
    int i;
    for(i=0; i<3; i++){
        
        // cout <<"Varsta: " << persoane[i]->varsta << endl;
        // cout <<"Varsta: " << (*persoane[i]).varsta << endl;
        cout << "INFO: " << *persoane[i] << endl;
    }
    
    cout << p3->varsta << endl;
    
    cout <<"----------------------------------------------------------" << endl;
    
    Persoana oPersoana;
    oPersoana.varsta = 44;
    cout << oPersoana << endl;
    cout << "O varsta: " << oPersoana.varsta << endl;
    
    Persoana * altaPersoana = new Persoana();
    altaPersoana->varsta = 45;
    cout <<"O alta varsta: " << altaPersoana->varsta << endl;
    cout << altaPersoana; // C nu genereaza erori chiar daca nu ai override-uit "<<" pentru ca stiue sa afiseze adresa
    cout << *altaPersoana;
    
    cout <<"----------------------------------------------------------" << endl;
    
    return 0;
}