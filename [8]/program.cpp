#include <iostream>

using namespace std;

class Task{
    public:
        void setFinished(bool f){
            finished = f;
        }
        
        bool getFinished(){
            return finished;
        }
    private:
        bool finished;
};


int main(){
    
    cout <<"Hello " << endl;
    
    Task t1;
    t1.setFinished(true);
    cout << t1.getFinished() << endl;
    
    bool valoareAdevar = true;
    cout <<valoareAdevar <<endl;
    
    
    /*
    int prop = -1;
    if(prop){
        cout << "CONDITIA E ADEVARATA " << endl;
    }else{
        cout << "CONDITIA NU PREA E ADEVARATA " << endl;
    }
    
    bool hasAccess = 99 > 10;
    if(hasAccess){
        cout <<"Welcome "<<endl;
    }else{
        cout <<"Go away"<<endl;
    }*/
    
    
    return 0;
    
}